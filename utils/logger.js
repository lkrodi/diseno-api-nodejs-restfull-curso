/**
 * logger: es un configurador para crear logs automaticos mediante un archivo .log
 * reemplazo del console.log. Requiere el modulo npm winston para su funcionamiento
 */

const winston = require("winston");

const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    winston.format.printf(msg => `[${msg.timestamp}] ${msg.level}: ${msg.message}`)
  ),
  transports:[
    new winston.transports.File({
      level: 'info',
      handleExceptions: true,
      maxsize:5120000, // 5 MB,
      maxFiles: 5,
      format: winston.format.combine(
        winston.format.colorize({message: true}),
      ),
      filename: `${__dirname}/../logs/logs-de-aplicacion.log`,
    }),
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: false,
      colorize: true,
      prettyPrint: object => { return JSON.stringify(object) }
    })
  ],
});

module.exports = logger;
