/**
 * Aqui se encuentran los Middlewares de Errores
 * Se centraliza en la @function procesarErrores utilizando Recursividad Invocada
 * Los @errors se emiten en el @catch luego de procesar la invocacion de una promesa
 */

const log = require("../../utils/logger");
const mongoose = require("mongoose");

exports.procesarErrores = (fn) => {
  return function(req, res, next) {
    fn(req, res, next).catch(next)
  }
}

exports.procesarErroresDb = (err, req, res, next) => {
  if (err instanceof mongoose || err.name === "MongoError") {
    log.error(`Ocurrio un error relacionado a mongoose. ${JSON.stringify(err)}`);
    err.message = "Error relacionado a la bases de datos, ocurrio inesperadamente. Para ayudarte contacta a rodolfokenlly@gmail.com";
    err.status = 500;
  }
  next(err);
}

exports.erroresProduccion = (err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    message: err.message
  });
}

exports.erroresDesarrollo = (err, req, res, next) => {
  res.status(err.status || 500)
  res.send({
    message: err.message,
    stack: err.stack
  });
}