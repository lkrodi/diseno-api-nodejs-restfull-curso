/**
 * Aqui la configuración para realizar la autenticacion.
 * Se utiliza el modulo de Passport Json Web tokens para generar la estrategia JWT
 */

//#region modulos NPM
const passportJwt = require("passport-jwt");
//#endregion

//#region Configuracion de Autenticacion y Recursos Externos de API
const log = require('../../utils/logger');
const config = require('../../config');
const UsuarioController = require("../recursos/usuarios/usuarios.controller");

const jwtOptions = {
  secretOrKey: config.jwt.secreto,
  jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken()
}
//#endregion

//#region Funciones de Estrategia Passport Autenticate
module.exports = new passportJwt.Strategy(jwtOptions, (jwtPayload, next) => {
  UsuarioController.obtenerUsuario({ id: jwtPayload.id }).then(usuario => {
    if (!usuario) {
      log.info(`JWT token no es válido, Usuario con Id: ${jwtPayload.id} no pudo ser autenticado`);
      next(null, false);
    } else {
      log.info(`Usuario ${usuario.username} suministró un token valido. Autenticación Exitosa.`);
      next(null, {
        username: usuario.username,
        id: usuario._id
      });
    }
  }).catch(error => {
    log.error("Error ocurrio al tratar de validar el token", error);
    next(error);
  });
});
//#endregion
