/**
 * Aqui se define los métodos/funciones para el modelo de datos de la tabla Usuarios
 */

const Usuario = require("./usuarios.model");

function crearUsuario(usuario, hashedPassword) {
  return new Usuario({
    username: usuario.username,
    password: hashedPassword,
    email: usuario.email
  }).save();
}

function usuarioExiste(username, email) {
  return new Promise((resolve, reject) => {
    Usuario.find().or([{ 'username': username }, { 'email': email }]).then(u => {
      resolve(u.length > 0);
    }).catch(error => {
      reject(error);
    });
  });
}

function obtenerUsuarios() {
  return Usuario.find();
}

function obtenerUsuario({ id, username }) {
  if (username) {
    return Usuario.findOne({ username: username });
  }
  if (id) {
    return Usuario.findById(id);
  }

  throw new Error("Funcion obtener usuarios del controller fue llamado sin tener username, ni id");
}

function borrarUsuario(id) {
  return Usuario.findByIdAndRemove(id);
}

function reemplazarUsuario(id, usuario, username) {
  return Usuario.findOneAndUpdate({ _id: id },{
    ...usuario
  }, {
    new: true
  });
}

module.exports = {
  crearUsuario,
  obtenerUsuarios,
  obtenerUsuario,
  borrarUsuario,
  reemplazarUsuario,
  usuarioExiste
};