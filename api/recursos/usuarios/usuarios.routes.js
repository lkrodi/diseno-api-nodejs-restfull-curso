//#region Modulos NPM
const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
//#endregion

//#region Configuracion  y Utilidades
const log = require("../../../utils/logger");
const config = require("../../../config");
const { procesarErrores } = require("../../libs/errorHandler");
//#endregion

//#region Recursos de Usuario
const UsuarioController = require("./usuarios.controller");
const { validateUsuario, validatePedidoLogin, transformarBodyLowerCase } = require("./usuarios.validate");
const { DatosDeUsuarioYaEnUso, CredencialesIncorrectas } = require("./usuarios.errors");
const usuariosRouter = express.Router();
//#endregion

//#region de API Rest Full de Usuarios
usuariosRouter.get('/',  procesarErrores(async (req, res) => {
  const usuariosDb = await UsuarioController.obtenerUsuarios();
  res.status(200).json(usuariosDb);
}));

usuariosRouter.get('/:id', procesarErrores(async (req, res) => {
  let { id } = req.params;
  let usuario;

  usuario = await UsuarioController.obtenerUsuario({id: id});

  if (!usuario) {
    log.warn(`El usuario con el Id: ${id} no existe`);
    res.status(404).send(`El usuario con el Id: ${id} no existe`);
    return;
  }

  res.status(200).json(usuario);
}));

usuariosRouter.post('/', [validateUsuario, transformarBodyLowerCase], procesarErrores((req, res) => {
  const { username, email, password } = req.body;

  return UsuarioController.usuarioExiste(username, email)
    .then(ue => {
      if (ue) {
        log.warn('El email o username ya estan asociados a una cuenta');
        throw new DatosDeUsuarioYaEnUso("El email o username ya estan asociados a una cuenta");
      }

      return bcrypt.hash(password, 10);
    }).then((hashPassword) => {
      return UsuarioController.crearUsuario(req.body, hashPassword)
        .then(nuevoUsuario => {
          res.status(201).send(`Usuario ${username} creado exitosamente`);
        })
    })
}));

usuariosRouter.post('/login', [validatePedidoLogin, transformarBodyLowerCase], procesarErrores(async (req, res) => {
  const { username, password } = req.body;
  let usuarioRegistrado, passwordCorrecto;

  usuarioRegistrado = await UsuarioController.obtenerUsuario({ username: username });

  if (!usuarioRegistrado) {
    log.info(`Usuario ${username} no existe. No pudo ser autenticado.`);
    throw new CredencialesIncorrectas();
  }

  passwordCorrecto = await bcrypt.compare(password, usuarioRegistrado.password);

  if (passwordCorrecto) {
    let token = jwt.sign({ id: usuarioRegistrado._id }, config.jwt.secreto, { expiresIn: config.jwt.tiempoExpiracion});
    log.info(`Usuario ${username} completó exitosamente la autenticacion.`);
    res.status(200).json({ token });
  }
  else {
    log.info(`Usuario ${username} no completó la autenticación. Su contraseña es incorrecta`);
    throw new CredencialesIncorrectas();
  }

}));

module.exports = usuariosRouter;