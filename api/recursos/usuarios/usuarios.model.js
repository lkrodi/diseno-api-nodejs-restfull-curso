/**
 * Aqui se define el modelo de datos para la tabla Usuarios
 */

const mongoose = require("mongoose");
const { Schema } = mongoose;

const usuarioSchema = new Schema({
  username: {
    type: String,
    minlength: 5,
    maxlength: 30,
    required: [true, 'Usuario debe tener un username']
  },
  password: {
    type: String,
    minlength: 5,
    maxlength: 120,
    required: [true, 'Usuario debe tener una contraseña']
  },
  email: {
    type: String,
    minlength: 5,
    required: [true, 'Usuario debe tener un email']
  }
});

module.exports = mongoose.model('usuario', usuarioSchema);