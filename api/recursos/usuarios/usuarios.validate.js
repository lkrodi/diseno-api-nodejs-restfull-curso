/**
 * Aqui se definen funciones de validacion y/o Middlewares de Express
 * Algunas veces se puede incluir el modulo Joi; sin embargo, no es obligatorio.
 */

//#region Modulos NPM y Utilidades
const Joi = require("joi");
const log = require("../../../utils/logger");
//#endregion

//#region Modelos de Validacion
const bluePrintUsuario = Joi.object().keys({
  username: Joi.string().alphanum().min(3).max(30).required(),
  password: Joi.string().min(6).max(200).required(),
  email: Joi.string().email().required()
});

const bluePrintUsuarioValidate = Joi.object().keys({
  username: Joi.string().required(),
  password: Joi.string().required()
});
 //#endregion

//#region Funciones de Validacion
function validateUsuario(req, res, next) {
  const resultValidate = Joi.validate(req.body, bluePrintUsuario, { abortEarly: false, convert: false });
  if (resultValidate.error === null) {
    next();
  } else {
    let errorsValidation = resultValidate.error.details.reduce((acumulador, error) => {
    return acumulador + `[${error.message}]`;
    }, "");
    log.warn(`Hubo errores en la validacion: ${errorsValidation}`);
    res.status(400).send(`Informacion debe tener lo siguiente: ${errorsValidation}`);
  }
}

function validatePedidoLogin(req, res, next) {
  const validate = Joi.validate(req.body, bluePrintUsuarioValidate, { abortEarly: false, convert: false });
  if (validate.error === null) {
    log.info(`El pedido del login fue validado exitosamente.`);
    next();
  }
  else {
    console.log(validate.error.details);
    log.info(`El login falló. Debe especificar un username y password. Ambos deben ser strings`);
    res.status(400).send(`El login falló. Debe especificar un username y password. Ambos deben ser strings`);
  }
}

function transformarBodyLowerCase(req, res, next) {
  req.body.username && (req.body.username = req.body.username.toLowerCase());
  req.body.email && (req.body.email = req.body.email.toLowerCase());
  next();
}
//#endregion

module.exports = {
  validateUsuario,
  validatePedidoLogin,
  transformarBodyLowerCase
};
