/**
 * Aqui van los errores de Productos generados mediante Clases ECMAScript
 * Consta de Un Mensaje, Status Http y un Name identificando el Error
 */

class ProductoNoExiste extends Error {
    constructor(message) {
      super(message)
      this.message = message || 'El producto no existe. La operacion no pudo ser completada'
      this.status = 409
      this.name = 'ProductoNoExiste'
    }
  }

  class UsuarioNoEsDuenio extends Error {
    constructor(message) {
      super(message);
      this.message = message || 'No eres dueño del producto. Operacion no pudo ser completada';
      this.status = 401;
      this.name = 'UsuarioNoEsDuenio';
    }
  }

  module.exports = {
    ProductoNoExiste,
    UsuarioNoEsDuenio
  }