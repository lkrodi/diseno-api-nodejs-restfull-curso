//#region Modulos NPM
const express = require("express");
const passport = require("passport");
//#endregion

//#region de Configuracion y Utilidades
const log = require("../../../utils/logger");
const jwtAutenticate = passport.authenticate('jwt', { session: false });
const { procesarErrores } = require("../../libs/errorHandler");
//#endregion

//#region de Recursos de Productos
const ProductoController = require("./productos.controller");
const { validateProducto, validarId } = require("./productos.validate");
const { ProductoNoExiste,UsuarioNoEsDuenio } = require("./productos.errors");
const productosRouter = express.Router();
//#endregion

//#region de API Rest Full de Productos
productosRouter.get('/', procesarErrores((req, res) => {
  return ProductoController.obtenerProductos()
    .then(productos => {
    res.status(200).json(productos);
  });
}));

productosRouter.get('/:id', validarId, procesarErrores((req, res) => {
  return ProductoController.obtenerProducto(req.params.id)
    .then(producto => {
      if (!producto) {
        log.info(`Producto con Id ${id} no existe`);
        throw new ProductoNoExiste(`Producto con Id ${id} no existe`);
      } else {
        res.status(200).json(producto);
      }
    });
}));

productosRouter.post('/', [jwtAutenticate, validateProducto], procesarErrores((req, res) => {
  return ProductoController.crearProducto(req.body, req.user.username).then(producto => {
    log.info("Producto se agrego a la base de datos.");
    res.status(201).json(producto);
  })
}));

productosRouter.put('/:id', [jwtAutenticate, validarId, validateProducto], procesarErrores(async (req, res) => {
  const { id } = req.params;
  const usuarioAutenticado = req.user.username;
  let productoReemplazar;

  productoReemplazar = await ProductoController.obtenerProducto(id);

  if (!productoReemplazar) {
    throw new ProductoNoExiste(`El producto con id ${id} no existe`);
  }

  if (productoReemplazar.duenio !== usuarioAutenticado) {
    log.warn(`Usuario ${usuarioAutenticado} no es dueño de producto con id [${id}]. Dueño real es [${productoReemplazar.duenio}]. Request no será procesado`);
    throw new UsuarioNoEsDuenio(`Usuario ${usuarioAutenticado} no es dueño de producto con id [${id}]. Dueño real es [${productoReemplazar.duenio}]. Request no será procesado`);
  }

  ProductoController.reemplazarProducto(id, req.body, usuarioAutenticado).then(producto => {
    log.info(`Producto con id ${id} reemplazado con nuevo producto ${JSON.stringify(producto)}`);
    res.status(200).json(producto);
  });
}));

productosRouter.delete('/:id', [jwtAutenticate, validarId], procesarErrores(async (req, res) => {
  let { id } = req.params,
    usuarioAutenticado = req.user.username,
    productoBorrar;

  productoBorrar = await ProductoController.obtenerProducto(id);

  if (!productoBorrar) {
    log.info(`Producto con Id: ${id} no fue encontrado`);
    throw new ProductoNoExiste(`Producto con Id: ${id} no fue encontrado`);
  }

  if (productoBorrar.duenio !== usuarioAutenticado) {
    log.info(`No eres dueño del producto ${productoBorrar.id}. Solo puedes eliminar productos tuyos, no de otros.`);
    throw new UsuarioNoEsDuenio(`No eres dueño del producto ${productoBorrar.id}. Solo puedes eliminar productos tuyos, no de otros.`);
  }

  let productoBorrado = await ProductoController.borrarProducto(id);
  log.info(`Producto con id ${id} fue borrado`);
  res.status(200).json(productoBorrado);
}));
//#endregion

module.exports = productosRouter;