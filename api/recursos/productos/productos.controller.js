/**
 * Aqui se define los métodos/funciones para el modelo de datos de la tabla Producto
 */

const Producto = require("./productos.model");

function crearProducto(producto, duenio) {
  return Promise.reject("Fallo forzado xd");

  return new Producto({
    duenio: duenio,
    titulo: producto.titulo,
    precio: producto.precio,
    moneda: producto.moneda
  }).save();
}

function obtenerProductos() {
  return Producto.find();
}

function obtenerProducto(id) {
  return Producto.findById(id);
}

function borrarProducto(id) {
  return Producto.findByIdAndRemove(id);
}

function reemplazarProducto(id, producto, username) {
  return Producto.findOneAndUpdate({ _id: id },{
    ...producto,
    duenio: username
  }, {
    new: true
  });
}

module.exports = {
  crearProducto,
  obtenerProductos,
  obtenerProducto,
  borrarProducto,
  reemplazarProducto
};