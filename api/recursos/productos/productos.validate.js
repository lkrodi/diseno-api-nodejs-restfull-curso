/**
 * Aqui se definen funciones de validacion y/o Middlewares de Express
 * Algunas veces se puede incluir el modulo Joi; sin embargo, no es obligatorio.
 */

 //#region Modulos NPM y Utilidades
 const Joi = require("joi");
 const log = require("../../../utils/logger");
 //#endregion

 //#region Modelos de Validacion
const bluePrintProducto = Joi.object().keys({
  titulo: Joi.string().max(100).required(),
  precio: Joi.number().positive().precision(2).required(),
  moneda: Joi.string().length(3).uppercase().required()
});
 //#endregion

//#region Funciones de Validacion
function validateProducto(req, res, next) {
  let resultValidate = Joi.validate(req.body, bluePrintProducto, { abortEarly: false, convert: false });
  if (resultValidate.error === null) {
    next();
  } else {
    let errorsValidation = resultValidate.error.details.reduce((acumulador, error) => {
      return acumulador + `[${error.message}]`;
    }, "");
    log.warn(`Hubo errores en la validacion: ${errorsValidation}`);
    res.status(400).send(`Errores en tu request: ${errorsValidation}`);
  }
}

function validarId(req, res, next) {
  let { id } = req.params;
  if (id.match(/^[a-fA-F0-9]{24}$/) === null) {
    log.info(`El id ${id} suministrado en el URL no es válido`);
    res.status(400).send(`El id ${id} suministrado en el URL no es válido`);
    return;
  }
  next();
}
//#endregion

//#region Exportables
module.exports = {
  validateProducto,
  validarId
}
//#endregion

