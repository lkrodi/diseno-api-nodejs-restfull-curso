/**
 * Aqui se define el modelo de datos para la tabla Producto
 */

const mongoose = require("mongoose");
const { Schema } = mongoose;

const productoSchema = new Schema({
  titulo: {
    type: String,
    required: [true, 'Producto debe tener un titulo']
  },
  precio: {
    type: Number,
    min: 0,
    required: [true, 'Producto debe tener un precio']
  },
  moneda: {
    type: String,
    maxlength: 3,
    minlength: 3,
    required: [true, 'Producto debe tener una moneda']
  },
  duenio: {
    type: String,
    required: [true, 'Producto debe estar asociado a un usuario']
  }
});

module.exports = mongoose.model('producto', productoSchema);