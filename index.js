//#region Modulos NPM
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const morgan = require("morgan");
const passport = require("passport");
//#endregion

//#region Modulos de Configuracion y Autenticacion
const authJwt = require('./api/libs/auth');
const config = require('./config');
const logger = require("./utils/logger");
const errorHandler = require("./api/libs/errorHandler");
//#endregion

//#region Modulos de API Routes
const productosRouter = require("./api/recursos/productos/productos.routes");
const usuariosRouter = require("./api/recursos/usuarios/usuarios.routes");
//#endregion

//#region Creacion de Servidor
const app = express();
//#endregion

//#region Conexion a la BD
mongoose.connect("mongodb://127.0.0.1:27017/apinodejs", { useNewUrlParser: true });
mongoose.connection.on('error', () => {
  logger.error("Falló la conexion a MongoDB");
  process.exit(1);
});
//#endregion

//#region Middlewares de Configuracion y Autenticacion
app.use(bodyParser.json());
app.use(morgan('short', { stream : { write: message => logger.info(message.trim()) }}));
passport.use(authJwt);
app.use(passport.initialize());
//#endregion

//#region Middlewares de API
app.use('/productos', productosRouter);
app.use('/usuarios', usuariosRouter);
//#endregion

//#region Inicializacion de Servidor
if (config.ambiente === 'prod') {
  app.use(errorHandler.erroresProduccion);
  logger.info("Procesaremos errores de Produccion");
} else {
  app.use(errorHandler.erroresDesarrollo)
  logger.info("Procesaremos errores de Desarrollo");
}
app.listen(config.puerto, () => {
  logger.info("Escuchando en el puerto 3000");
});
//#endregion