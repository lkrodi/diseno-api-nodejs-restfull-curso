/**
 * Aqui va la configuracion para definir y detectar el tipo de entorno(ENV)
 */

const ambiente = process.env.NODE_ENV || 'development';
const configuracionBase = {
  jwt: {},
  puerto: 3000
};

let configuracionAmbiente = {};

switch(ambiente) {
  case 'desarrollo':
  case 'dev':
  case 'development':
    configuracionAmbiente = require("./dev");
    break;
  case 'produccion':
  case 'prod':
    configuracionAmbiente = require("./prod");
    break;
  default:
    configuracionAmbiente = require("./dev");
}

module.exports = {
  ...configuracionBase,
  ...configuracionAmbiente
};