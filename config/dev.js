/**
 * Aqui va la configuracion para el entorno de desarrollo.
 */

module.exports = {
  jwt: {
    secreto: 'secretodedesarrollo',
    tiempoExpiracion: '24h'
  }
};