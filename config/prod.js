/**
 * Aqui va la configuracion para el entorno de produccion.
 */

module.exports = {
  jwt: {
    secreto: 'secretodeproduccion',
    tiempoExpiracion: '12h'
  }
};